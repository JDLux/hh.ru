import requests
from bs4 import BeautifulSoup
import fake_useragent
import time 
import pandas as pd
from openpyxl import load_workbook

def get_links(text):
    ua =fake_useragent.UserAgent()
    res = requests.get(
        url=f"https://hh.ru/search/vacancy?area=1&area=113&search_field=name&search_field=company_name&search_field=description&enable_snippets=true&only_with_salary=true&text={text}&page=1",
        headers={"user-agent":ua.random}
    )
    if res.status_code != 200:
        return
    soup = BeautifulSoup(res.content, "lxml")
    try:
        page_count = int(soup.find("div",attrs={"class":"pager"}).find_all("span",recursive=False)[-1].find("a").find("span").text)
    except:
        return
    for page in range(page_count):
        try:
            res = requests.get(
                url=f"https://hh.ru/search/vacancy?area=1&area=113&search_field=name&search_field=company_name&search_field=description&enable_snippets=true&only_with_salary=true&text={text}&page={page}",
                headers={"user-agent":ua.random}
            )
            if res.status_code == 200:
                soup = BeautifulSoup(res.content, "lxml")
                for a in soup.find_all("a",attrs={"class":"serp-item__title"}):
                    yield f'{a.attrs["href"].split("?")[0]}'
        except Exception as e:
            print(f"{e}")
        time.sleep(1)


class vacancy:
    name = ""
    salary = ""

def get_vacancy(link):
    vac = vacancy() 
    ua =fake_useragent.UserAgent()
    data = requests.get(
        url=link,
        headers={"user-agent":ua.random}
    )
    if data.status_code != 200:
        return
    soup = BeautifulSoup(data.content, "lxml")
    try:
        vac.name = soup.find(attrs={"class":"bloko-header-section-1"}).text
    except:
        vac.name = ""
    try:
        vac.salary = soup.find(attrs={"data-qa":"vacancy-salary-compensation-type-net"}).text.replace("\xa0", "").replace("на руки", "").replace("до", " - ").replace("от", "")
    except:
        vac.salary = ""
        
    return vac


if __name__ == "__main__":
    
    vachelp= vacancy
    
    languages = ["Visual Basic", "PHP", "Java", "C", "Python", "JavaScript", "Assembly", "SQL"]
    
    fn = 'rez.xlsx'
    wb = load_workbook(fn)
    ws = wb['data']
    
    for i in range(8):
        for a in get_links(languages[i]):
            vachelp = get_vacancy(a)
            if (vachelp.name != "" and vachelp.salary != ""):
                time.sleep(1)
                print(vachelp.name, vachelp.salary)
                ws.append([vachelp.name, vachelp.salary])
                wb.save(fn) 
            
    wb.close
